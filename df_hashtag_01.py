from itertools import groupby
from turtle import pd
import pandas as pd

## Import/load data frame
pandas = pd.read_json('./data/data_frame.json')
# print(pandas)

## hapus nilai Nan di data 
pandas.dropna(axis=0, inplace=True)

df = pd.DataFrame() ## Buat data frame tampung nilai/element baru
for i in range(len(pandas)): ## Mencari nilai data 
    row = pandas.iloc[i] ## Mencari value nilai
    user_id = row['performerId']
    interestBy = row['interestBy']
    hashtag_list = row['hashtag'].split()
    
    for hashtag in hashtag_list: ## Buat Elenen/data kedalam data frame
        element_df = pd.DataFrame({'performerId': [user_id], 'hashtag': [hashtag], 'interestBy': [interestBy]})
        df = pd.concat([df, element_df]) ## Gabungkan/tampilkan data 

        # df.reset_index(inplace = True, drop = True) ## hapus kolam index
        # print(df)

result = df.groupby(['hashtag','performerId','interestBy'])['hashtag'].count()
print(result)